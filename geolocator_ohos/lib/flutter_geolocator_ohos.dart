export 'package:geolocator_platform_interface/geolocator_platform_interface.dart'
    show
    ActivityMissingException,
    AlreadySubscribedException,
    InvalidPermissionException,
    LocationAccuracy,
    LocationAccuracyStatus,
    LocationPermission,
    LocationServiceDisabledException,
    LocationSettings,
    PermissionDefinitionsNotFoundException,
    PermissionDeniedException,
    PermissionRequestInProgressException,
    Position,
    PositionUpdateException,
    ServiceStatus;

export 'src/flutter_geolocator_ohos.dart';
export 'src/types/ohos_settings.dart' show OhosSettings;
export 'src/types/foreground_settings.dart'
    show OhosResource, ForegroundNotificationConfig;
